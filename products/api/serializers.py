from rest_framework import serializers

from products.models import Photo, Feature
from ..models import Product


class PhotoSerializer(serializers.ModelSerializer):
    preview = serializers.ImageField()

    class Meta:
        model = Photo
        fields = ('image', 'preview', 'date_created', 'date_updated')


class FeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feature
        fields = ('type', 'value')


class ProductListSerializer(serializers.ModelSerializer):
    cover = PhotoSerializer()

    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'old_price', 'cover',
                  'date_created', 'date_updated',)


class ProductSerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(many=True)
    features = FeatureSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'price', 'old_price',
                  'date_created', 'date_updated', 'photos', 'features')
