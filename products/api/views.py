from drf_yasg.inspectors import SwaggerAutoSchema
from rest_framework import generics, filters

from main.utils import add_desc_ordering
from .serializers import ProductSerializer, ProductListSerializer
from ..models import Product

PRODUCT_LIST_ORDERING_FIELDS = ['name', 'price', 'date_created', 'date_updated']


class EnumOrderingAutoSchema(SwaggerAutoSchema):
    def add_manual_parameters(self, parameters):
        result = super().add_manual_parameters(parameters)
        for parameter in result:
            if parameter.name == 'ordering':
                parameter.description = f'{parameter.description} If the first character is "-" then sort in DESC order.'
                parameter.enum = add_desc_ordering(PRODUCT_LIST_ORDERING_FIELDS)
            elif parameter.name == 'search':
                parameter.description = 'A search by name.'
        return result


class ProductList(generics.ListAPIView):
    """
    Список товаров с фильтром по имени и сортировкой
    """
    queryset = Product.objects.filter(published=True)
    serializer_class = ProductListSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = PRODUCT_LIST_ORDERING_FIELDS
    ordering = ['-date_created']
    search_fields = ['name']
    swagger_schema = EnumOrderingAutoSchema


class ProductDetail(generics.RetrieveAPIView):
    """
    Детальная информация о товаре
    """
    queryset = Product.objects.filter(published=True)
    serializer_class = ProductSerializer
