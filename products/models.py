from contextlib import suppress

from django.db import models
from django.utils.translation import gettext as _
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.fields import ThumbnailerImageField


def upload_product_photos_to(instance, filename):
    return 'uploads/products/{0}/{1}'.format(instance.product.id, filename)


class Product(models.Model):
    name = models.CharField(max_length=1024, verbose_name=_('название'))
    description = models.TextField(blank=True, verbose_name=_('описание'))
    price = models.DecimalField(max_digits=10,
                                decimal_places=2,
                                verbose_name=_('цена'))
    old_price = models.DecimalField(max_digits=10,
                                    decimal_places=2,
                                    null=True,
                                    blank=True,
                                    verbose_name=_('старая цена'))
    published = models.BooleanField(default=True,
                                    verbose_name=_('опубликовано'))
    date_created = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_('дата создания'))
    date_updated = models.DateTimeField(auto_now=True,
                                        verbose_name=_('дата обновления'))

    class Meta:
        verbose_name = _('продукт')
        verbose_name_plural = _('продукты')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.old_price:
            self.old_price = self.price

        super().save(*args, **kwargs)

    @property
    def cover(self):
        return self.photos.first()


class Photo(models.Model):
    image = ThumbnailerImageField(upload_to=upload_product_photos_to,
                                  verbose_name=_('изображение'))
    product = models.ForeignKey(Product,
                                related_name='photos',
                                on_delete=models.CASCADE,
                                verbose_name=_('продукт'))
    date_created = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_('дата создания'))
    date_updated = models.DateTimeField(auto_now=True,
                                        verbose_name=_('дата обновления'))

    def __str__(self):
        return self.image.name

    @property
    def preview(self):
        with suppress(InvalidImageFormatError):
            return self.image['500x500']
        return self.image


class Feature(models.Model):
    FEATURE_COLOR = 'color'
    FEATURE_SIZE = 'size'
    FEATURE_EQUIPMENT = 'equipment'
    FEATURE_MANUFACTURER = 'manufacturer'
    FEATURE_COUNTRY = 'country'
    FEATURE_LIST = (
        (FEATURE_COLOR, _('цвет')),
        (FEATURE_SIZE, _('размеры')),
        (FEATURE_EQUIPMENT, _('комплектация')),
        (FEATURE_MANUFACTURER, _('производитель')),
        (FEATURE_COUNTRY, _('страна')),
    )

    type = models.CharField(max_length=256,
                            choices=FEATURE_LIST,
                            verbose_name=_('тип'))
    value = models.CharField(max_length=1024,
                             verbose_name=_('значение'))
    product = models.ForeignKey(Product,
                                related_name='features',
                                on_delete=models.CASCADE,
                                verbose_name=_('продукт'))
