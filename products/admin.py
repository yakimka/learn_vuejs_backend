from django.contrib import admin

from .models import Product, Photo, Feature


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 1


class FeatureInline(admin.TabularInline):
    model = Feature
    extra = 1


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [PhotoInline, FeatureInline]
    list_display = ('id', 'name', 'published', 'date_created')
    list_display_links = ('id', 'name')
    list_editable = ('published',)
    ordering = ('date_created',)

    def save_model(self, request, obj, form, change):
        obj.save()

        for afile in request.FILES.getlist('photos_multiple'):
            obj.photos.create(image=afile)
