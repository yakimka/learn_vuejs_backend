def add_desc_ordering(ordering):
    return list(ordering) + [f'-{ord}' for ord in ordering]
